$ = jQuery;

// Preloader

// $(window).on('load', function () {
//     setTimeout(function(){
//         var $preloader = $('#page-preloader'),
//             $spinner   = $preloader.find('#nest');
//         $spinner.fadeOut();
//         $preloader.fadeOut('slow');
//     }, 500);
// });

$(document).ready(function(){

    // MAIN nAVIGATION
    var Nav = new hcOffcanvasNav('#main-nav', {
        disableAt: false,
        customToggle: '.toggle',
        levelSpacing: 5,
        navTitle: 'MENU',
        levelTitles: true,
        levelTitleAsBack: false,
        pushContent: '#container',
        labelClose: false
    });

    $('.content_toggle').click(function(){
		$(this).parent().find('.content_block').toggleClass('hide');
		return false;
	});

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    // Calendar
    if($(".datepicker-here").length){
        $('.datepicker-here').datepicker({
            language: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
                monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                today: 'Today',
                clear: 'Clear',
                dateFormat: 'mm/dd/yyyy',
                timeFormat: 'hh:ii aa',
                firstDay: 0
            }
        });
    }


    // Animation
    new WOW().init();

    $(window).on('load', function () {

        //breadcrumb HEIGHT FIX
        var navHeight = $("body #Navbar").innerHeight();
        $(".breadcrumb_wrapper").css("padding-top",navHeight);


        window.requestAnimationFrame(function() {

            // setTimeout(function() {
            //     var onscreen = new TimelineMax({repeat:0, yoyo:false});
            //         onscreen.from(".navbar", .5, {y:-100, ease: "none"})
            //         onscreen.to("#slider", 1, {opacity: 1, ease: "none"})
            //         onscreen.fromTo(".fixed_butttons", 1, {x:50, opacity: 0, ease: "none"},{x:0, opacity: 1, ease: "none"},"-=1")
            // }, 300)

        });

        $("#owl-store").owlCarousel({
            loop: false,
            items: 2,
            margin: 30,
            stagePadding: 0,
            center: true,
            autoWidth: true,
            nav: true,
            navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
            dots: false,
            autoplay: false,
              autoplayTimeout: 10000,
            autoplayHoverPause: false,
            mouseDrag: true,
            touchDrag: true,
            responsive:{
                768:{
                    items:1,
                    autoWidth: false
                },
                1200:{
                    items:2
                }
            }
        });
        
    });
    
    // Fancybox

	$('[data-fancybox]').fancybox({
        touch: false,
        animationDuration : 600,
        animationEffect   : 'zoom-in-out'
	});

    // MAIN TOP SLIDER
    
    $("#slider").owlCarousel({
        animateOut: '',
        animateIn: '',
        smartSpeed: 450,
        loop: true,
        items: 1,
        margin: 0,
        nav: true,
        navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        dots: false,
        autoplay: false,
      	autoplayTimeout: 10000,
        mouseDrag: false,
        touchDrag: false,
        responsive: {
            0:{
                items:1
            }
        }
    });

    $("#owl-partners").owlCarousel({
        loop: false,
        items: 6,
        margin: 10,
        stagePadding: 0,
        center: false,
        autoWidth: false,
        nav: true,
        navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        dots: false,
        autoplay: false,
          autoplayTimeout: 10000,
        autoplayHoverPause: false,
        mouseDrag: true,
        touchDrag: true,
        responsive:{
            0:{
                items:2,
            },
            568:{
                items:3,
            },
            768:{
                items:4,
            },
            992:{
                items:5,
            },
            1200:{
                items:6
            }
        }
    });

    //MOBILE STORE CAROUSEL
    
    $("#owl-store-mobile").owlCarousel({
        loop: false,
        items: 1,
        margin: 30,
        stagePadding: 0,
        center: false,
        autoWidth: false,
        nav: true,
        navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
        dots: false,
        autoplay: false,
      	autoplayTimeout: 10000,
        autoplayHoverPause: false,
        mouseDrag: true,
        touchDrag: true
    });

    // FormStyler

    setTimeout(function() {
        $('.searchable').styler({
            selectSearch: true,
            selectSearchLimit: 10,
            locale: 'en',
            locales: {
                'en': {
                    selectSearchNotFound: 'No matches found',
                    selectSearchPlaceholder: 'Search...'
                }
            },
        });
		$('.selectable').styler({
            locale: 'en',
            locales: {
                'en': {
                    filePlaceholder: 'Choose File',
                    fileBrowse: 'Browse',
                    fileNumber: 'Selected files: %s',
                    selectPlaceholder: 'Select...',
                    selectSearchNotFound: 'No matches found',
                    selectSearchPlaceholder: 'Search...'
                }
            },
        });
	}, 100)

    // Filter Category

    function cat_filter() {
        var category = $('#select_category').val();
        $('#select_subcategory > option:not(:first-child)').attr('disabled', true).hide();
        $('#select_subcategory > option').each(function() {
            if ($(this).data('category') == category) {
                $(this).attr('disabled', false).show();
            }
        });
        if (category === '') {
            $('#select_subcategory').addClass('disabled').attr('disabled', true);
            $('.selectable').trigger('refresh');
        } else {
            $('#select_subcategory').removeClass('disabled').attr('disabled', false);
            $('.selectable').trigger('refresh');
        }
        $('.selectable').trigger('refresh');
    }

    $('#select_category').on('change', function() {
        $('#select_subcategory').prop('selectedIndex', 0);
        cat_filter();
    });



    //Travel Insurance Program CALCULATOR

    $('#subject').on('change', function () {

        if($(this).val() == 'subject1') {
            $('#option1').addClass('d-none');
            $('#option2').addClass('d-none');
        }

        if($(this).val() == 'subject2') {
            $('#option1').removeClass('d-none');
            $('#option2').addClass('d-none');
        }

        if($(this).val() == 'subject3') {
            $('#option1').removeClass('d-none');
            $('#option2').removeClass('d-none');
        }
    });


    // Calculator

    if($(".steps-fields").length>0) {
        $(".steps-fields .scroll-to").click(function () {
            $('html, body').animate({
                scrollTop: $('.steps-fields').parent().offset().top
            }, 200);
        });
    }

    $('#travel_type label').on('click',function () {
        $('#travel_type label').removeClass('btn-danger');
        $('#travel_type label').addClass('btn-outline-secondary');
        $(this).addClass('btn-danger');
        $(this).removeClass('btn-outline-secondary');
    });

    $('#coverage_type label').on('click',function () {
        $('#coverage_type label').removeClass('btn-danger');
        $('#coverage_type label').addClass('btn-outline-secondary');
        $(this).addClass('btn-danger');
        $(this).removeClass('btn-outline-secondary');
        if($('#coverage_type input:checked').val() == '1') {
            $('#type1').removeClass('d-none');
            $('#type1 .schengen').removeClass('d-none');
        }
        if($('#coverage_type input:checked').val() != '1') {
            $('#type1').removeClass('d-none');
            $('#type1 .schengen').addClass('d-none');
        }
    });

    $(".next-step-button").on("click", function(){
        $(this).parents(".step_wrapper").find(".collapsed-step").addClass("filled");
    });

    $('.calc_summary .btn_info').on("click", function(){
        $(this).toggleClass('open').next().slideToggle();
    });

    jQuery(".dropdown-menu").innerWidth(jQuery(".dashboard_menu").width()-2);
    jQuery(".dropdown-menu").innerWidth(jQuery(".medical_menu").width()-2);

    $('.filter_head').on('click', function(){
        $(this).next().slideToggle();
    });

});